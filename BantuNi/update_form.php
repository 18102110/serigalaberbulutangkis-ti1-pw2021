<?php
  include "koneksi.php";
  $qpetani = "select * from petani";
  $data_petani = $conn->query($qpetani);
  $qkomentar = "SELECT k.komentar_id, k.foto, k.nama_lengkap, k.isi_komentar, k.petani_id, p.nama
                    FROM komentar AS k
                    JOIN petani AS p
                    ON p.petani_id = k.petani_id";
  $data_komentar = $conn->query($qkomentar);

  $qselect_komentar = "SELECT * FROM komentar 
                        LEFT JOIN petani 
                        ON petani.petani_id = komentar.petani_id
                        WHERE komentar_id = ".$_GET['komentar_id'];
  foreach($conn->query($qselect_komentar) as $value){
  $data_select_komentar=$value;
  } ; 
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous" />

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" />

    <!-- CSS -->
    <link rel="stylesheet" href="style.css" />

    <title>BantuNi</title>
    <link rel="icon" href="img/icon2.png" />
  </head>
  <body>
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top" style="background-color: #f3f6fa">
      <div class="container">
        <a class="navbar-brand text-primary mb-0 h1" href="index.html">BantuNi</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ms-auto fw-normal">
            <li class="nav-item">
              <a class="nav-link" href="index.html#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.html#service">Service</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active fw-bold" aria-current="page" href="blog.html">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="index.html#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Akhir Navbar -->

    <!-- Arikel 1 -->
    <section id="artikel1">
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="row">

              <div class="col-md align-self-center ">

                <p class="fs-1 fw-bold lh-sm">Jenis-jenis hama pada tanaman kopi</p>
                <p class="lh-sm mt-3 mb-4" style="text-align: justify; text-indent: 0.5in;">Tahukah kamu bahwa terdapat banyak jenis hama yang sangat mudah menyerang tanaman kopi? Terlebih lagi, jika kopi yang ditanam kurang memiliki perawatan sehingga akhirnya tanaman kopi akan rusak yang tentunya mampu menurunkan produktivitas kopi tersebut. Jenis hama yang paling sering ditemukan oleh para petani adalah hama yang kerap menyerang kopi, yang bisa menyebabkan pembusukan dan gugurnya bunga rontok dan buah muda. Jika masalah hama tidak segera ditangani dengan cepat, maka hasil panen yang didapatkan juga akan menurun. 
                Nah, apa saja jenis-jenis hama yang sering menyerang tanaman kopi dan bagaimana mengatasinya:</p>
              </div>

              <div class="col-md mb-4 align-self-center">
                <div class="card">
                  <img src="img/kopi1.jpg" class="card-img-top" alt="petani 2">
                </div>
              </div>

            </div>

            <div class="row">
              <div class="col">
                <div class="row">
                  
                  <div class="col-md">
                    <p class="fs-5 fw-bold lh-sm">1. Hama Bubuk Buah Kopi</p>
                    <p class="mb-4 lh-sm" style="text-align: justify; text-indent: 0.5in; " > Hama ini biasanya adalah sebuah kumbang kecil yang hanya menyerang buah kopi baik yang masih muda maupun yang sudah tua. Untuk mengendalikan hama ini, petani dapat memotong batang buah yang banyak terserang atau mengumpulkan buah untuk kemudian dimusnahkan. Selain itu, petani juga bisa menggunakan pestisida Dimecron 50 CW, Tamaron, dan Argothion.</p>

                    <p class="fs-5 fw-bold lh-sm">2. Hama Bubuk Cabang Batang Kopi</p>
                    <p class="lh-sm" style="text-align: justify; text-indent: 0.5in;">Hama ini menyerang bagian batang tanaman kopi, biasanya pada bagian ranting kecil yang baru memiliki diameter 3 cm dan bagian pucuk tanaman muda. Gejala yang muncul jika hama ini sudah menyerang adalah daun menjadi rontok, daun berubah menjadi kuning, dan batang kering yang khusus ada pucuk.
                    Untuk mengatasi hama ini, petani bisa menyemprotkan larutan pestisida secara rutin sampai hama benar-benar hilang total.</p>
                  </div>

                  <div class="col-md">
                    <p class="fs-5 fw-bold lh-sm">3. Penyakit Kerat Daun Tanaman Kopi</p>
                    <p class="mb-4 lh-sm" style="text-align: justify; text-indent: 0.5in;">Penyakit ini biasanya disebabkan oleh cendawan dimana pada bagian daun kopi terdapat bercak merah dan bintik-bintik. Gejala lainnya adalah daun yang menguning di bagian bawah permukaan daun, bercak kuning dan daun gugur, bagian cabang muda kopi kering, buah kopi berubah menjadi hitam, serta kopi terlihat lebih pucat.Petani bisa mengatasi penyakit kerat daun pada tanaman kopi ini dengan menggunakan pestisida Fungisidaditane dengan dosis pemakaian 2 gram untuk 2 liter air.</p>

                    <p class="fs-5 fw-bold lh-sm">4. Hama Kutu Dompolan Putih dan Hijau</p>
                    <p class="lh-sm" style="text-align: justify; text-indent: 0.5in;">Hama ini menyerang bagian buah kopi hingga buahnya terlihat seperti terlilit bubuk putih. Biasanya, hama ini menyerang karena adanya pohon naungan yang terlalu gelap. Untuk mengendalikannya, potong sebagian ranting pohon di sekitar tanaman kopi supaya membuka celah udara dan sinar matahari untuk menyinari kopi. Selain itu, petani juga bisa mengggunakan pestisida Poxindo 50 WP.</p>
                  </div>

                </div>
              </div>
              
            </div>

            <div class="row">
              <div class="col">
                <p class="fs-3 fw-bold lh-sm mb-3">Jenis-Jenis Serangga</p>

                <!-- Serangga1 -->
                <p class="fs-5 fw-bold lh-sm">1. Serangga Hama PBKo</p>
                <p class="mb-4 lh-sm" style="text-align: justify; text-indent: 0.5in;">Serangga ini membuat buah kopi menjadi berlubang yang kerugiannya bisa mencapai 50%. Cara pengendaliannya adalah dengan menggunakan perangkap serangga.</p>

                <!-- Serangga2 -->
                <p class="fs-5 fw-bold lh-sm">2. Nematoda Endoparasit</p>
                <p class="mb-4 lh-sm" style="text-align: justify; text-indent: 0.5in;">Jika serangga ini menyerang tanaman kopi, gejala yang muncul berupa kopi terlihat kerdil, daun menguning dan gugu, bunga prematur dan banyak yang kosong, serta bagian akar membusuk.
                Cara mengatasi serangga ini adalah dengan menggunakan varietas kopi yang tahan terhadap nematoda parasit dan dengan cara kultur teknis, membuat lubang tanam rotasi dan parit barier.</p>

                <!-- Serangga3 -->
                <p class="fs-5 fw-bold lh-sm">3. Kutu Dompolan</p>
                <p class="mb-4 lh-sm" style="text-align: justify; text-indent: 0.5in;">Kutu Dompolan menyerang tanaman kopi dengan menghisap cairan kuncup bunga. Akibatnya adalah daun-daun menguning, calon bunga gagal, dan buah menjadi rontok.
                Pengendaliannya adalah dengan melepaskan parasit Anagyrus Grenii atau menyemprotkan insektisida Anthio 330 EC.</p>

                <!-- Serangga4 -->
                <p class="fs-5 fw-bold lh-sm">4. Hypothenemus Hampei</p>
                <p class="mb-4 lh-sm" style="text-align: justify; text-indent: 0.5in;">Serangga ini adalah serangga dewasa berwarna coklat hitam dengan panjang betina sekitar 2 mm dan jantan 1,3 mm. Disebut juga dengan serangga penggerek buah kopi atau bubuk buah kopi. Serangga ini akan masuk ke dalam buah dengan membuat lubang di sekitar diskus.
                Pengendalian Hypothenemus Hampei dapat dilakukan dengan menggunakan parasitoid Cephalonomia Stephanoderis.</p>
                
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!-- Akhir artikel -->

    <!-- Komentar -->
    <section id="contact">
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md mb-4 align-self-center">
            <div class="card">
              <img src="img/kopi2.jpg" class="card-img-top" alt="petani 2">
            </div>
          </div>

          <div class="col-md">
            <h6 class="fw-bold text-primary">BERI KOMENTAR</h6>
            <form action ="simpan_komentar.php" method="POST" enctype="multipart/form-data" >
            <input type="hidden" name="komentar_id" value="<?php echo $data_select_komentar['komentar_id'] ?>">
            <div class="mb-3">
                <label for="nama_lengkap">Nama Lengkap</label>
                <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" value="<?php echo $data_select_komentar['nama_lengkap'] ?>" required>
                </div>          
                <div class="mb-3">
                    <label for="petani">Petani</label>
                    <select class="custom-select d-block w-100" id="Petani" name="petani_id" required>
                        <option value="">Pilih...</option>
                        <?php
                            foreach($data_petani as $index => $value){
                                if($data_select_komentar['petani_id']==$value['petani_id']){
                                    $is_selected = 'selected';
                                }
                                else{
                                    $is_selected = '';
                                }
                        ?>
                            <option <?php echo $is_selected ?> value="<?php echo $value['petani_id'] ?>"><?php echo $value['nama'] ?></option>
                        <?php
                         }
                        ?>
                    </select>
                </div>
                <div class="mb-3">
                  <label for="isi_komentar">Komentar Anda</label>
                  <textarea type="text"  class="form-control" id="isi_komentar" name="isi_komentar" required rows="3"><?php echo $data_select_komentar['isi_komentar'] ?></textarea>               
                </div>
                <label for="gambar">Pilih Gambar</label>
                <input type="file" name="gambar" id="gambar">
                <br>
                <br>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Update Komentar</button>
                <a href="artikel1.php" class="btn btn-warning btn-lg btn-block" type="submit">Batal</a>
            </form>

          </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir Komentar -->

    <!-- Tampil Komentar -->
    <section id="about">
      <div class="container">
        <div class="row justify-content-center">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-muted">Komentar</span>
          </h4>
      <?php
          foreach($data_komentar as $index => $value){
      ?>
     <ul class="list-group mb-3">
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div class="container">
                <div class="row">
		            <div class="row">
                <div> 
                    <img src="uploads/<?php echo $value['foto'];?>" width="80px" height="80px">
                  </div>
                    <div class="col-10">
                      <h6 class="my-0"><?php echo $value['nama_lengkap'] ?></h6>
		                  <p class="text-muted"><em><?php echo $value['nama'] ?></em></p>
                      
                    </div>
                    <div class="col">
                      <a href="update_form.php?komentar_id=<?php echo $value['komentar_id'] ?>" type="button" class="close"><i class="bi bi-pencil"></i></a>
                    </div>
                    <div class="col">
                      <a href="hapus_komentar.php?komentar_id=<?php echo $value['komentar_id'] ?>" type="button" class="close"><i class="bi bi-trash"></i></a>
                    </div>
		              </div>
		                <div class="row">
                    <div class="col-8">
                    <small><?php echo $value['isi_komentar'] ?></small>
                    </div>
                </div>

              </div>
            </li>
          </ul>
      <?php
        }
      ?>
          </div>
         </div>
        </div>
      </div>
    </section>
    <!-- Akhir tampil komentar -->

    <!-- Awal Footer -->
    <section id="footer">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md mb-3">
            <div class="row justify-content-evenly">
              <div class="col-3">
                <h5 class="text-primary fw-bold">BantuNi</h5>
              </div>
              <div class="col-7">
                <h7 class="fw-bold mb-2">Latest Blog Post</h7>
                <p class="fs-5 fw-bold lh-sm mt-1">Bagaimana cara mengatasi hama tikus?</p>
                <p class="lh-sm mt-3" style="text-align: justify; text-indent: 0.5in;">Tikus adalah hama yang terpenting pada tanaman padi di Indonesia. Hama ini harus diperhatikan khusus. Karena kehilangan hasil produksi akibat serangan hama tikus sangat tinggi. 
                  Usaha untuk mengendalikan tikus ini sudah banyak dilakukan oleh para petani, mulai dari fisik, cara hayati, sanitasi, kultur teknik, mekanik dan kimia. Tetapi diakui, bahwa dengan cara pengendalian itu belum optimal, sehingga harapan untuk menekan populasi tikus sangatlah sulit.</p>
              </div>
            </div>
          </div>

          <div class="col">
            <div class="row justify-content-evenly">
              <div class="col-md-4">
                <h7 class="fw-bold mb-2">Service</h7>
                <p class="mt-1">Jenis & Serangan Hama <br />Pencegahan & Penanggulangan Hama</p>
              </div>
              <div class="col-md-3">
                <h7 class="fw-bold mb-2">Company</h7>
                <p class="mt-1">About Us <br />Blog <br />Contact</p>
              </div>
              <div class="col-md">
                <h7 class="fw-bold mb-2">Support</h7>
                <p class="mt-1">Privacy Police <br />Blog <br />Contact</p>
              </div>
            </div>

            <div class="row mt-5 justify-content-start">
              <div class="col-md-4">
                <p>Copyright 2021-2021</p>
              </div>
              <div class="col-md-4">Privacy - Terms</div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Akhir Footer -->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>
